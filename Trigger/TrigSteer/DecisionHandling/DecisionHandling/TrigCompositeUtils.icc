/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "AthenaKernel/getMessageSvc.h"

namespace TrigCompositeUtils {

  /**
   * @brief Creates and right away records the Container CONT with the key.
   * No Aux store.
   * Returns the WriteHandle. 
   * If possible provide the context that comes via an argument to execute otherwise it will default to looking it up which is slower.
   **/
  template<class CONT>
  SG::WriteHandle<CONT> createAndStoreNoAux( const SG::WriteHandleKey<CONT>& key, const EventContext& ctx ) {
    SG::WriteHandle<CONT> handle( key, ctx );
    auto data = std::make_unique<CONT>() ;
    if (handle.record( std::move( data ) ).isFailure()) {
      throw std::runtime_error( "ERROR in TrigCompositeUtils::createAndStoreNoAux Unable to record " + key.key());
    }
    return handle;
  }

  /**
   * @brief Creates and right away records the Container CONT with the key.
   * With Aux store.
   * Returns the WriteHandle. 
   * If possible provide the context that comes via an argument to execute otherwise it will default to looking it up which is slower.
   **/
  template<class CONT, class AUX>
  SG::WriteHandle<CONT> createAndStoreWithAux( const SG::WriteHandleKey<CONT>& key, const EventContext& ctx ) {
    SG::WriteHandle<CONT> handle( key, ctx );
    auto data = std::make_unique<CONT>() ;
    auto aux = std::make_unique<AUX>() ;
    data->setStore( aux.get() );
    if (handle.record( std::move( data ), std::move( aux )  ).isFailure()) {
      throw std::runtime_error( "ERROR in TrigCompositeUtils::createAndStoreWithAux Unable to record " + key.key());
    }
    return handle;
  }

  template<typename T>
  void
  findLinks(const xAOD::TrigComposite* start, const std::string& linkName, std::vector<LinkInfo<T>>& links) {
    ElementLinkVector<T> featureLinks;
    if (start->hasObjectCollectionLinks(linkName, ClassID_traits<T>::ID())) {
      featureLinks = start->objectCollectionLinks<T>(linkName);
    }
    if (start->hasObjectLink(linkName, ClassID_traits<T>::ID())) {
      featureLinks.push_back(start->objectLink<T>(linkName));
    }
    for (const ElementLink<T>& featureLink : featureLinks) {
      // Check for duplicates
      if (std::none_of(links.begin(), links.end(), [&](const auto& li) { return (li.link == featureLink); } ))
      {
        links.emplace_back(start, featureLink);
      }
    }
    // Recursive
    for (const auto& seed : getLinkToPrevious(start)) {
      findLinks<T>(*seed, linkName, links);
    }
  }

  template<typename T>
  std::vector<LinkInfo<T>>
  findLinks(const xAOD::TrigComposite* start, const std::string& linkName) {
    std::vector<LinkInfo<T>> links;
    findLinks(start, linkName, links);
    return links;
  }

  template<typename T>
  LinkInfo<T>
  findLink(const xAOD::TrigComposite* start, const std::string& linkName, const bool suppressMultipleLinksWarning) {
    std::vector<LinkInfo<T>> links = findLinks<T>(start, linkName);
    if (links.size() > 1 && !suppressMultipleLinksWarning) {
      MsgStream(Athena::getMessageSvc(), "TrigCompositeUtils::findLink") << MSG::WARNING
        << links.size() << " links found for " << linkName
        << " returning the first link, consider using findLinks." << endmsg;
    }
    if (links.size() > 0) {
      return links.at(0);
    }
    return LinkInfo<T>(); // invalid link
  }

  template<class CONTAINER>
  const std::vector< LinkInfo<CONTAINER> > getFeaturesOfType(
    const std::vector<ElementLinkVector<DecisionContainer>>& linkVector,
    const bool oneFeaturePerLeg,
    const std::string& featureName) {

    std::vector< LinkInfo<CONTAINER> > features;
    // For each unique path through the navigation for a given chain
    for (const ElementLinkVector<DecisionContainer>& decisionPath : linkVector) {
      // For each step along this path, starting at the terminus and working back towards L1
      for (const ElementLink<DecisionContainer>& decisionLink : decisionPath) {
        const Decision* decision = (*decisionLink);

        ElementLinkVector<CONTAINER> featureLinks;
        // Slices may have added link collections
        if (decision->hasObjectCollectionLinks(featureName, ClassID_traits< CONTAINER >::ID())) {
          featureLinks = decision->objectCollectionLinks<CONTAINER>( featureName );
        }
        // Framework specified "feature" is always a single link
        if (decision->hasObjectLink(featureName, ClassID_traits< CONTAINER >::ID())) {
          featureLinks.push_back( decision->objectLink<CONTAINER>( featureName ) );
        }

        // This try block protects against ExcCLIDMismatch throws from 
        // features which do not derive from IParticle, when an IParticle interface is requested.
        try {
          for (const ElementLink<CONTAINER>& featureLink : featureLinks) {
            if (std::none_of(features.begin(), features.end(), [&](const auto& li) { return li.link == featureLink; } )) {
              // Don't duplicate up returned features (may have entries from many chains in the linkVector)
              features.push_back( LinkInfo<CONTAINER>(decision, featureLink) );
            }
          }
          if (featureLinks.size() && oneFeaturePerLeg) {
            break;
          }
        } catch (SG::ExcCLIDMismatch&) {
          // This is in place to catch the exception caused by non-IParticle features when an IParticle interface is requested.
          // We're fine to catch this silently and cary on looking at the next Decision object in the graph
        }

      } // for (decisionLink : decisionPath)
    } // for (decisionPath : linkVector)
    return features;
  }

}
