#include "../TrigTileRODMuAlg.h"
#include "../TrigTileLookForMuAlg.h"
#include "../TrigTileMuToNtuple.h"
#include "../TrigTileMonAlg.h"
#include "../TrigTileMuFex.h"

DECLARE_COMPONENT( TrigTileRODMuAlg )
DECLARE_COMPONENT( TrigTileLookForMuAlg )
DECLARE_COMPONENT( TrigTileMuToNtuple )
DECLARE_COMPONENT( TrigTileMonAlg )
DECLARE_COMPONENT( TrigTileMuFex )
